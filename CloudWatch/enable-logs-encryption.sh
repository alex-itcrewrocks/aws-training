LOG_GROUP="<LOG-GROUP-ARN>"
KMS_ID="<KMS-ARN>"

# associate with existing log group
aws logs associate-kms-key --log-group-name $LOG_GROUP --kms-key-id $KMS_ID

# create new log group
aws logs create-log-group --log-group-name $LOG_GROUP --kms-key-id $KMS_ID 
