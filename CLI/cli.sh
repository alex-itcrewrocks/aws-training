# dry-run

aws ec2 run-instances --image=ami-0eb7496c2e0403237 --instance-type t2.micro --dry-run --region eu-central-1

# see what it happens if token expired or permissions are added/removed

# decode the error message

aws sts decode-authorization-message --encoded-message <ERROR-MESSAGE>

# see EC2 metadata

curl http://169.254.169.254/latest/meta-data

# MFA

device=`aws iam list-mfa-devices --query 'MFADevices[*].SerialNumber' --output text`
aws sts get-session-token --serial-number $device --token-code $token \
--query "Credentials.{AccessKeyId:AccessKeyId,SecretAccessKey:SecretAccessKey,SessionToken:SessionToken}"
