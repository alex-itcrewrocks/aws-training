# Get latest AMI

aws ssm get-parameters --names /aws/service/ecs/optimized-ami/amazon-linux-2/recommended/image_id --region us-west-2

# Get specific parameters

aws ssm get-parameters --names /test/rds-user /test/rds-pass --region us-west-2

# Get specific parameters and decrypt

aws ssm get-parameters --names /test/rds-user /test/rds-pass --region us-west-2 --with-decryption

# Get parameters by path

aws ssm get-parameters-by-path --path /test/